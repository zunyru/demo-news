<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;

class NewsControllerDemo extends Controller
{
    public function index()
    {
        $news = News::query()->with([
            'categories',
            'tags'
        ])->find(1);

        //$news = News::query()->find(1);
        //dd($news->categories[0]->pivot->category);
        $data = [];
        foreach ($news as $item_news) {
            foreach ($item_news->categories as $item) {
                $data[$item->news->title][] = $item->category->name;
            }
        }

        return view('pages.home');
    }

    public function addTag()
    {
        $news = News::find(6);
        $news->syncTags(['นายก']);
    }

    public function getNews($id)
    {
        $item_news = News::query()
            ->with([
                'tags',
                'category_news.news',
                'category_news.category',])
            ->find($id);
        $data = [];

        if (sizeof($item_news->category_news) != 0) {
            foreach ($item_news->category_news as $item) {
                $data[$item->news->title]['category'][] = $item->category->name;
                $data[$item->news->title]['tags'] = $item_news->tags->pluck('name')->all();
            }
        } else {
            $data[$item_news->title]['category'] = [];
            $data[$item_news->title]['tags'] = $item_news->tags->pluck('name')->all();
        }

        dd($data);
    }

    public function addNews()
    {
        $news = new News();
        $news->title = 'EEEE';
        $news->slug = 'eeee';
        $news->hero_banner = 'E';
        $news->save();

        $news->syncTags(['นายก', 'ประเทศไทย', 'ทักษิณ2', 'ภาคใต้']);
    }
}
