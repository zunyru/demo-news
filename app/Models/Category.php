<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory, SoftDeletes;

    /*public function category_news()
    {
        return $this->hasMany(CategoryNews::class);
    }*/


    public function news(): BelongsToMany
    {
        return $this->belongsToMany(News::class)->using(CategoryNews::class);
    }
}
