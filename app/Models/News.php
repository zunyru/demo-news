<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Spatie\Tags\HasTags;
use Spatie\Tags\Tag;

class News extends Model
{
    use HasFactory, HasTags;

    protected $fillable = [
        'title',
        'details'
    ];

    /*public function category_news()
    {
        return $this->hasMany(CategoryNews::class);
    }*/

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class)->using(CategoryNews::class);
    }

    public static function getTagClassName(): string
    {
        return Tag::class;
    }

    public function tags(): MorphToMany
    {
        return $this
            ->morphToMany(self::getTagClassName(), 'taggable', 'taggables', null, 'tag_id')
            ->orderBy('order_column');
    }
}
