<x-layout>
    <x-slot:title>
        Home
        </x-slot>
        <div class="container mt-24">
            <h1 class="text-center">
                สร้างข่าว
            </h1>

            <form action="{{ route('news.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="grid grid-cols-1 gap-6 px-10 pb-10">

                    <label class="block">
                        <span class="text-gray-700">รูปข่าว</span>

                        <input type="file"
                               name="hero_banner">

                    </label>

                    <label class="block">
                        <span class="text-gray-700">หัวข้อข่าว</span>
                        <input type="text"
                               name="title"
                               class="
                    mt-1
                    block
                    w-full
                    rounded-md
                    border-gray-300
                    shadow-sm
                    focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50
                  " placeholder="" spellcheck="false" data-ms-editor="true">
                    </label>

                    <label class="block">
                        <span class="text-gray-700">ประเภทข่าว</span>
                        <select
                                name="category_id[]"
                                multiple
                                class="
                                select2
                    block
                    w-full
                    mt-1
                    rounded-md
                    border-gray-300
                    shadow-sm
                    focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50
                  ">
                            <option value="">--กรุณาเลือกประเภทข่าว--</option>
                            @foreach($categories as $key => $category)
                                <option value="{{ $key }}">{{ $category }}</option>
                            @endforeach
                        </select>
                    </label>
                    <label class="block">
                        <span class="text-gray-700">เนื้อหาข่าว</span>
                        <textarea
                                name="details"
                                class="summernote
                                        mt-1
                                        block
                                        w-full
                                        rounded-md
                                        border-gray-300
                                        shadow-sm
                    focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50
                  " rows="3" spellcheck="false" data-ms-editor="true"></textarea>
                    </label>

                    <label class="block">
                        <span class="text-gray-700">Tags</span>
                        <select class="form-control select2-tags"
                                name="tags[]"
                                multiple="multiple">
                        </select>
                    </label>

                    <div class="mt-8 text-center">
                        <button class="px-10 py-1 bg-green-500 text-white rounded-md"
                                type="submit">
                            บันทึก
                        </button>
                    </div>

                </div>
            </form>

        </div>

        @push('script')
            <script>
                $(document).ready(function () {
                    $('.summernote').summernote({
                        height: 300,
                    });

                    $('.select2').select2();

                    $(".select2-tags").select2({
                        tags: true,
                        tokenSeparators: [',', ' ']
                    })
                });
            </script>
    @endpush
</x-layout>
