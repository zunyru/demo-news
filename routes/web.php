<?php

use App\Http\Controllers\NewsController;
use \Illuminate\Support\Facades\Route;


Route::get('news/create', [NewsController::class, 'create'])
    ->name('news.create');

Route::post('news', [NewsController::class, 'store'])
    ->name('news.store');

//Route::resource('news', NewsController::class);