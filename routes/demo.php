<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('pages.home');
})->name('home.index');

Route::any('/my-test', function () {
    return "TEST";
})->name('home.any');*/

Route::name('home.')->group(function () {
    Route::get('/', function () {
        return view('pages.home');
    })->name('index');

    Route::any('/my-test', function () {
        return "TEST";
    })->name('any');

    Route::get('/my-name', function () {
        return "TEST";
    })->name('my-name');
});

Route::view('/welcome', 'pages.home');

Route::get('/user/{id}/names/{name}', function ($id, $name) {
    return 'User :' . $id . ' Name :' . $name;
})->whereNumber('id')->whereAlphaNumeric('name');

Route::get('/category/{category}', function ($category) {
    return $category;
})->whereIn('category', ['ชาย', 'หญิง']);


Route::get('/search/{search}', function ($search) {
    return $search;
})->where('search', '.*');


Route::get('news-demo', [\App\Http\Controllers\NewsControllerDemo::class, 'index']);

Route::get('news-demo-addtag', [\App\Http\Controllers\NewsControllerDemo::class, 'addTag']);

Route::get('news-demo-tags/{id}', [\App\Http\Controllers\NewsControllerDemo::class, 'getNews']);

Route::get('news-demo-adds', [\App\Http\Controllers\NewsControllerDemo::class, 'addNews']);
